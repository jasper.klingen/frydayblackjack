import vue from 'vue'
import vuex from 'vuex'

vue.use(vuex);
export default new vuex.Store({
    state: {
        playerName : "",
        deckId: "",
        dealerCards: [],
        playerCards: []
    },
    mutations: {
        setPlayerName: (state, payload) => {
            state.playerName = payload;
        },
        setDeckId: (state, payload) => {
            state.deckId = payload;
        },
        setDealerCard: (state, payload) => {
            state.dealerCards = [...state.dealerCards, ...payload];
        },
        setPlayerCard: (state, payload) => {
            state.playerCards = [...state.playerCards, ...payload];
        },
    },
    getters: {

    },
    actions: {
        async getDeck({ commit }) {
            const response = await fetch(`https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1`)
            const { deck_id } = await response.json()
            
            commit('setDeckId', deck_id)
        },
        async drawCard({ state, commit }, cardDetails) {
            const response = await fetch(`https://deckofcardsapi.com/api/deck/${state.deckId}/draw/?count=${cardDetails.numberOfCards}`)
            const { cards } = await response.json()
            
            if(cardDetails.isDealer) {
                commit('setDealerCard', cards)
            } else {
                commit('setPlayerCard', cards)
            }
            
        }
    }
    
})